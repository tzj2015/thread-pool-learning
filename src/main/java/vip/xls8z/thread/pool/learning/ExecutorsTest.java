package vip.xls8z.thread.pool.learning;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class ExecutorsTest {
    public static void main(String[] args) {
        // 1. 创建固定线程池
        /*
         * 底层队列使用 LinkedBlockingQueue
         * 存在问题：导致堆积大量任务，导致OOM，因为队列的容量是Integer.MAX_VALUE
         */
        ExecutorService fixedThreadPool = Executors.newFixedThreadPool(10);

        // 2. 创建缓存线程池，对线程池内线程超过60s没有使用，从线程池中移除
        /*
         * 底层队列使用 SynchronousQueue
         * 存在问题：SynchronousQueue 特点是：
         *      - 无缓冲等待队列，队列容量为 "1"，会直接将任务交给工作线程处理
         *      - 阻塞队列中任务必须被工作线程消费之后才能继续添加任务，使用SynchronousQueue一般要求
         */
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();

        // 3. 创建定时任务延时任务线程池
        ExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(100);

        // 4. 创建单个线程线程池
        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();

    }
}
